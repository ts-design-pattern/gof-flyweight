/**
 * 專案名稱： gof-flyweight
 * 檔案說明： 抽象查詢服務
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Observable } from 'rxjs';

/**
 * 抽象查詢服務
 */
export interface QueryService<T = any> {
  /**
   * 享元模式
   */
  flyweight: Map<string, T | T[]>;

  /**
   * 透過特定條件查詢
   *
   * @method public
   * @param condition 查詢條件
   * @return 回傳查詢符合條件的資料
   */
  findBy(condition: string): Observable<T | T[]>;
}
