/**
 * 專案名稱： gof-flyweight
 * 檔案說明： 較佳的開發模式程式進入點
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { AppComponent } from './app.component';

const app = new AppComponent();

app.onSiteChange('WKS');
app.onSiteChange('WKS');
