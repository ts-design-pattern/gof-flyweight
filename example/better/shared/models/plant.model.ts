/**
 * 專案名稱： gof-flyweight
 * 檔案說明： 廠別資料模型
 * -----------------------------------------------------------------------------
 * @NOTE
 */

/**
 * 廠別資料模型
 */
export interface Plant {
  /**
   * 流水號
   */
  id: number;
  /**
   * Site
   */
  site: string;
  /**
   * 廠別代碼
   */
  plant: string;
}
