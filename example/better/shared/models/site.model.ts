/**
 * 專案名稱： gof-flyweight
 * 檔案說明： Site 資料模型匯出點
 * -----------------------------------------------------------------------------
 * @NOTE
 */

/**
 * Site 資料模型匯出點
 */
export interface Site {
  /**
   * 流水號
   */
  id: number;
  /**
   * Site
   */
  site: string;
}
