/**
 * 專案名稱： gof-flyweight
 * 檔案說明： 廠別 API
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Observable, of } from 'rxjs';
import { Plant } from '../models';
import * as PLANT from './../../../../mock/data/plant.json';

/**
 * 廠別 API
 */
export class PlantApi {
  /**
   * 查詢特定 Site 下的廠別資料
   *
   * @method public
   * @param site Site
   * @return 回傳特定 Site 下的廠別資料
   */
  public findBySite(site: string): Observable<Plant[]> {
    console.log('call plant by api');
    return of(PLANT.filter(plant => plant.site === site));
  }
}
