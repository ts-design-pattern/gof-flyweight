/**
 * 專案名稱： gof-flyweight
 * 檔案說明： Site API
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { from, Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import * as SITE from './../../../../mock/data/site.json';
import { Site } from './../models';

/**
 * Site API
 */
export class SiteApi {
  /**
   * 查詢特定 Site 資料
   *
   * @method public
   * @param site Site
   * @return 回傳特定 Site 資料
   */
  public findBy(site: string): Observable<Site> {
    console.log('call site by api');
    return from(SITE).pipe(filter(s => s.site === site));
  }
}
