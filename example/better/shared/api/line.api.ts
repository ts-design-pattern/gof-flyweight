/**
 * 專案名稱： gof-flyweight
 * 檔案說明： 線別 API
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Observable, of } from 'rxjs';
import { Line } from '../models';
import * as LINE from './../../../../mock/data/line.json';

/**
 * 線別 API
 */
export class LineApi {
  /**
   * 查詢特定廠別下的廠別資料
   *
   * @method public
   * @param plant 廠別
   * @return 回傳特定廠別下的廠別資料
   */
  public findByPlant(plant: string): Observable<Line[]> {
    console.log('call line by api');
    return of(LINE.filter(line => line.plant === plant));
  }
}
