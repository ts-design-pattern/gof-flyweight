/**
 * 專案名稱： gof-flyweight
 * 檔案說明： 線別查詢服務
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LineApi } from '../api';
import { Line } from '../models';
import { QueryService } from './../../core';

/**
 * 線別查詢服務
 */
export class LineService implements QueryService<Line> {
  /**
   * 享元模式
   */
  public flyweight: Map<string, Line[]> = new Map<string, Line[]>();

  /**
   * @param _lineApi 線別 API
   */
  constructor(private _lineApi: LineApi = new LineApi()) {}

  /**
   * 查詢特定廠別下的廠別資料
   *
   * @method public
   * @param plant 廠別
   * @return 回傳特定廠別下的廠別資料
   */
  public findBy(plant: string): Observable<Line[]> {
    const existLines = this.flyweight.get(plant);
    if (existLines) {
      return of(existLines);
    } else {
      return this._lineApi
        .findByPlant(plant)
        .pipe(tap(lines => this.flyweight.set(plant, lines)));
    }
  }
}
