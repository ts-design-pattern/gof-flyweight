/**
 * 專案名稱： gof-flyweight
 * 檔案說明： Site 查詢服務
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { QueryService } from '../../core';
import { SiteApi } from '../api';
import { Site } from '../models';

/**
 * Site 查詢服務
 */
export class SiteService implements QueryService<Site> {
  /**
   * 享元模式
   */
  public flyweight: Map<string, Site> = new Map<string, Site>();

  /**
   * @param _siteApi Site API
   */
  constructor(private _siteApi: SiteApi = new SiteApi()) {}

  /**
   * 查詢特定 Site 資料
   *
   * @method public
   * @param site Site
   * @return 回傳特定 Site 資料
   */
  public findBy(site: string): Observable<Site> {
    const existSite = this.flyweight.get(site);
    if (existSite) {
      return of(existSite);
    } else {
      return this._siteApi
        .findBy(site)
        .pipe(tap(s => this.flyweight.set(site, s)));
    }
  }
}
