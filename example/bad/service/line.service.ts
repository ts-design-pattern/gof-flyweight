/**
 * 專案名稱： gof-flyweight
 * 檔案說明： 線別查詢服務
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Observable } from 'rxjs';
import { LineApi } from '../api';
import { Line } from '../models';

/**
 * 線別查詢服務
 */
export class LineService {
  /**
   * @param _lineApi 線別 API
   */
  constructor(private _lineApi: LineApi = new LineApi()) {}

  /**
   * 查詢特定廠別下的廠別資料
   *
   * @method public
   * @param plant 廠別
   * @return 回傳特定廠別下的廠別資料
   */
  public findBy(plant: string): Observable<Line[]> {
    return this._lineApi.findByPlant(plant);
  }
}
