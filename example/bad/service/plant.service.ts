/**
 * 專案名稱： gof-flyweight
 * 檔案說明： 廠別查詢服務
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Observable } from 'rxjs';
import { PlantApi } from '../api';
import { Plant } from '../models';

/**
 * 廠別查詢服務
 */
export class PlantService {
  /**
   * @param _plantApi 廠別 API
   */
  constructor(private _plantApi: PlantApi = new PlantApi()) {}

  /**
   * 查詢特定 Site 下的廠別資料
   *
   * @method public
   * @param site Site
   * @return 回傳特定 Site 下的廠別資料
   */
  public findBy(site: string): Observable<Plant[]> {
    return this._plantApi.findBySite(site);
  }
}
