/**
 * 專案名稱： gof-flyweight
 * 檔案說明： Site 查詢服務
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Observable } from 'rxjs';
import { SiteApi } from '../api';
import { Site } from '../models';

/**
 * Site 查詢服務
 */
export class SiteService {
  /**
   * @param _siteApi Site API
   */
  constructor(private _siteApi: SiteApi = new SiteApi()) {}

  /**
   * 查詢特定 Site 資料
   *
   * @method public
   * @param site Site
   * @return 回傳特定 Site 資料
   */
  public findBy(site: string): Observable<Site> {
    return this._siteApi.findBy(site);
  }
}
