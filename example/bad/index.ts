/**
 * 專案名稱： gof-flyweight
 * 檔案說明： 不好的範例程式進入點
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { AppComponent } from './app.component';

const app = new AppComponent();

app.onSiteChange('WKS');
app.onSiteChange('WKS');
