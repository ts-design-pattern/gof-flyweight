/**
 * 專案名稱： gof-flyweight
 * 檔案說明： APP 元件
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Line, Plant } from './models';
import { LineService, PlantService, SiteService } from './service';

/**
 * APP 元件
 */
export class AppComponent {
  /**
   * 廠別下拉選單清單
   */
  public plantList: Plant[] = [];
  /**
   * 線別下拉選單清單
   */
  public lineList: Line[] = [];

  /**
   * @param _siteService  Site 查詢服務
   * @param _plantService 廠別查詢服務
   * @param _lineService  線別查詢服務
   */
  constructor(
    private _siteService: SiteService = new SiteService(),
    private _plantService: PlantService = new PlantService(),
    private _lineService: LineService = new LineService(),
  ) {}

  /**
   * 當 Site 發生變化時
   *
   * @method public
   * @param site Site
   */
  public async onSiteChange(site: string): Promise<void> {
    this.plantList = await this._plantService.findBy(site).toPromise();
    console.log(this.plantList.map(plant => plant.plant).join(','));
  }
}
