/**
 * 專案名稱： gof-flyweight
 * 檔案說明： 抽象使用者查詢服務
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Observable } from 'rxjs';
import { User } from './models';

/**
 * 抽象使用者查詢服務
 */
export interface UserService {
  /**
   * 使用者享元
   */
  userFlyweight: Map<string, User>;

  /**
   * 透過 ID 查詢特定使用者資料
   *
   * @method public
   * @param userId 使用者 ID
   * @return 回傳特定 ID 的使用者資料
   */
  findById(userId: string): Observable<User>;
}
