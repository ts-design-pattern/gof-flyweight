/**
 * 專案名稱： gof-flyweight
 * 檔案說明： 使用者資料模型
 * -----------------------------------------------------------------------------
 * @NOTE
 */

/**
 * 使用者資料模型
 */
export interface User {
  /**
   * 使用者 ID
   */
  userId: string;
  /**
   * 使用者名稱
   */
  username: string;
  /**
   * Email
   */
  email: string;
}
