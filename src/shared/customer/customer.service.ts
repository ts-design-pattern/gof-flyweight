/**
 * 專案名稱： gof-flyweight
 * 檔案說明： 客戶查詢服務
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { UserApi } from '../api';
import { User, UserService } from './../../core';

/**
 * 客戶查詢服務
 */
export class CustomerService implements UserService {
  /**
   * 使用者享元
   */
  public userFlyweight = new Map<string, User>();

  /**
   * @param _userApi 使用者 API
   */
  constructor(private _userApi: UserApi = new UserApi()) {}

  /**
   * 透過 ID 查詢特定使用者資料
   *
   * @method public
   * @param userId 使用者 ID
   * @return 回傳特定 ID 的使用者資料
   */
  public findById(userId: string): Observable<User> {
    const existUser = this.userFlyweight.get(userId);
    if (existUser) {
      return of(existUser);
    } else {
      return this._userApi
        .findById(userId)
        .pipe(tap(user => this.userFlyweight.set(user.userId, user)));
    }
  }
}
