/**
 * 專案名稱： gof-flyweight
 * 檔案說明： 使用者 API
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { from, Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import * as users from '../../../mock/data/users.json';
import { User } from '../../core';

/**
 * 使用者 API
 */
export class UserApi {
  /**
   * 透過 ID 查詢特定使用者資料
   *
   * @method public
   * @param userId 使用者 ID
   * @return 回傳特定 ID 的使用者資料
   */
  public findById(userId: string): Observable<User> {
    console.log('call by api');
    return from(users).pipe(filter(user => user.userId === userId));
  }
}
