/**
 * 專案名稱： gof-flyweight
 * 檔案說明： 享元模式範例程式進入點
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { CustomerService } from './shared';

(async () => {
  const costomerService = new CustomerService();
  let user = await costomerService.findById('S100').toPromise();
  console.log(`fetch user ${user?.username} first time`);
  user = await costomerService.findById('S100').toPromise();
  console.log(`fetch user ${user?.username} second time`);
})();
